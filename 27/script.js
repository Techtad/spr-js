window.addEventListener("DOMContentLoaded", function (event) {
    var N = 64

    var table = []
    for (let i = 1; i <= N; i++) {
        let num
        do {
            num = Math.floor(Math.random() * N) + 1
        } while (table.includes(num))
        table[i] = num
    }
    table.sort((a, b) => { return parseInt(b) - parseInt(a) })

    document.body.style.whiteSpace = "pre"
    for (let i = 1; i <= N; i++) {
        document.body.innerHTML += table[i - 1] + " "
        if (i % 10 == 0)
            document.body.innerHTML += "<br>"
    }
})