window.addEventListener("DOMContentLoaded", function (event) {
    let button = document.querySelector("button")
    let input = document.querySelector("[name='znak']")

    button.addEventListener("click", (event) => {
        let n = Math.floor(Math.random() * (90 - 65 + 1)) + 65
        input.value = String.fromCharCode(n)
    })
})