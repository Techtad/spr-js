var borderStyles = [
    "none",
    "hidden",
    "dotted",
    "dashed",
    "solid",
    "double",
    "groove",
    "ridge",
    "inset",
    "outset"
]
var index = 0

window.addEventListener("DOMContentLoaded", function (event) {
    document.getElementById("area").style.borderWidth = "5px"
    document.getElementById("area").addEventListener("keydown", function (event) {
        event.preventDefault()
        document.getElementById("area").style.borderStyle = borderStyles[(++index) % borderStyles.length]
    })
})