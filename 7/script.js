window.addEventListener("DOMContentLoaded", function (event) {
    let f = document.getElementById("formularz")
    let input = f.querySelector("input")
    let button = f.querySelector("button")

    button.onclick = function (event) {
        event.preventDefault()
        let addr = input.value.includes("http") ? input.value : "http://" + input.value
        window.location = addr
    }
})