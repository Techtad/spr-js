window.addEventListener("DOMContentLoaded", function (event) {
    document.querySelectorAll("input").forEach((input) => {
        input.addEventListener("focus", (event) => {
            event.target.style.backgroundColor = "#888888"
        })
        input.addEventListener("blur", (event) => {
            event.target.style.backgroundColor = "white"
        })
    })
})