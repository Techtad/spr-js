window.addEventListener("DOMContentLoaded", function (event) {
    let N
    do {
        N = prompt("ile elementów utworzyć?")
    } while (isNaN(N))
    N = parseInt(N)

    for (let i = 0; i < N; i++) {
        let div = document.createElement("div")
        div.classList.add("klasa")
        document.body.append(div)
    }
})