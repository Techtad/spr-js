var SETTINGS = {
    IMG_WIDTH: 50,
    IMG_HEIGHT: 50
}

class Kolo {
    constructor(speed) {
        this.speed = speed
    }

    wstaw(element) {
        this.img = document.createElement("img")
        this.img.src = "round.png"

        let x = Math.round(Math.random() * (window.innerWidth - (SETTINGS.IMG_WIDTH*2))) + SETTINGS.IMG_WIDTH
        let y = Math.round(Math.random() * (window.innerHeight - (SETTINGS.IMG_HEIGHT*2))) + SETTINGS.IMG_HEIGHT
        this.img.style.position = "absolute"
        this.img.style.left = x + "px"
        this.img.style.top = y + "px"

        this.img.style.animationDuration = (1 / this.speed) + "s"

        element.append(this.img)
    }

    obracaj() {
        if(this.img && !this.img.classList.contains("rotating"))
            this.img.classList.add("rotating")
    }
}

window.addEventListener("DOMContentLoaded", function (event) {
    let dodajKolo = function() {
        let k = new Kolo(Math.random() * 5)
        k.wstaw(document.body)
        k.obracaj()
        setTimeout(dodajKolo, 2000)
    }
    dodajKolo()
})