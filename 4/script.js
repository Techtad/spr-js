window.addEventListener("DOMContentLoaded", function (event) {
    let spans = document.querySelectorAll(".data")
    let date = new Date()
    date.setDate(date.getDate() + 1)
    while (date.getDay() != 1)
        date.setDate(date.getDate() + 1)

    for (let span of spans) {
        let dateParts = date.toLocaleDateString("PL", { year: "2-digit", month: "2-digit", day: "2-digit" }).split(".")
        let day = dateParts[0]
        let month = dateParts[1]
        let year = dateParts[2]
        span.innerText = `${year}-${month}-${day}`
        date.setDate(date.getDate() + 1)
    }
})