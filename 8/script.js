window.addEventListener("DOMContentLoaded", function (event) {
    let input = document.body.getElementsByTagName("input")[0]
    input.addEventListener("keydown", function (event) {
        if (event.code != "Enter") return
        event.preventDefault()
        if (!isNaN(input.value)) {
            if (parseInt(input.value) <= 0) {
                alert("wpisz liczbę większą od zera")
                return
            }
            input.disabled = true
            let decrement = function () {
                input.value = parseInt(input.value) - 1
                if (parseInt(input.value) > 0)
                    setTimeout(decrement, 500)
                else
                    input.disabled = false
            }
            decrement()
        } else {
            input.value = ""
            alert("wpisz liczbę")
        }
    })
})