window.addEventListener("DOMContentLoaded", function (event) {
    document.querySelectorAll("div").forEach((div) => {
        div.addEventListener("click", (event) => {
            let style = window.getComputedStyle(event.target)
            let shade = 255 - (parseFloat(event.offsetX) / parseFloat(style.width) * 255)

            event.target.style.backgroundColor = `rgb(${shade}, ${shade}, ${shade})`
        })
    })
})