var changeColors1 = false
var changeColors2 = false
var on2 = false
var div1, div2

function change1Red() {
    if (changeColors1) {
        div1.style.backgroundColor = "red"
        setTimeout(change2Blue, 2000)
    }
}

function change2Red() {
    if (changeColors2) {
        div2.style.backgroundColor = "red"
        setTimeout(change1Blue, 2000)
    }
}

function change1Blue() {
    if (changeColors2)
        div1.style.backgroundColor = "blue"
}

function change2Blue() {
    if (changeColors1)
        div2.style.backgroundColor = "blue"
}

window.addEventListener("DOMContentLoaded", function (event) {
    let divs = document.getElementsByTagName("div")
    div1 = divs[0]
    div2 = divs[1]

    div1.addEventListener("mouseover", (event) => {
        if (on2) return
        changeColors1 = true
        setTimeout(change1Red, 1000)
    })
    div2.addEventListener("mouseover", (event) => {
        event.stopPropagation()
        on2 = true
        changeColors2 = true
        setTimeout(change2Red, 1000)
    })

    div1.addEventListener("mouseout", (event) => {
        changeColors1 = false
        clearTimeout(change1Blue)
        clearTimeout(change1Red)
        clearTimeout(change2Blue)
        clearTimeout(change2Red)
        div1.style.backgroundColor = "white"
        div2.style.backgroundColor = "white"
    })
    div2.addEventListener("mouseout", (event) => {
        changeColors2 = false
        clearTimeout(change1Blue)
        clearTimeout(change1Red)
        clearTimeout(change2Blue)
        clearTimeout(change2Red)
        div1.style.backgroundColor = "white"
        div2.style.backgroundColor = "white"
        on2 = false
    })
})