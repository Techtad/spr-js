var letters = ['a', 'b', 'c', 'd', 'e']
var i = 0
window.addEventListener("DOMContentLoaded", function (event) {
    let move = function () {
        document.body.append(document.querySelector(`img[src='imgs/${letters[i]}.png']`))
        i = ++i % letters.length

        setTimeout(move, 1000)
    }

    setTimeout(move, 1000)
})