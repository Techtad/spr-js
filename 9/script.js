window.addEventListener("DOMContentLoaded", function (event) {
    for (let i = 0; i < 5; i++) {
        let button = document.createElement("button")
        button.innerText = i
        button.addEventListener("click", function (event) {
            alert(event.target.innerText)
        })
        document.body.append(button)
    }
})