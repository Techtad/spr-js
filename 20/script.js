window.addEventListener("DOMContentLoaded", function (event) {
    window.addEventListener("contextmenu", (event) => {
        event.preventDefault()

        let div = document.createElement("div")
        div.style.position = "absolute"
        div.style.left = event.clientX + "px"
        div.style.top = event.clientY + "px"
        div.innerText = "Nie skopiujesz!"
        document.body.appendChild(div)

        setTimeout(function () {
            this.remove()
        }.bind(div), 5000)
    })
})