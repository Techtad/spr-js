window.addEventListener("DOMContentLoaded", function (event) {
	var formularz = document.getElementById("formularz")
	var inputLogin = formularz.querySelector("[placeholder='login']")
	var inputPassword = formularz.querySelector("[placeholder='password']")
	var inputSubmit = formularz.querySelector("[type='submit']")

	inputSubmit.addEventListener("click", function (event) {
		event.preventDefault()
		if (inputLogin.value.length == 0 || inputPassword.value.length == 0)
			alert("wypełnij formularz")
		else
			formularz.submit()
	})
})